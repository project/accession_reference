# Accession Reference field type

A Drupal project implementing a field type for Accession References, a
museum term describing the ID used to label a specific item in the museum's
collection.

This particular field implements an accession reference with two text fields,
both of which are numeric, with a separator character (default '/') between
them. While this could perhaps have been done using e.g. the double\_field
project, a new type enables additional functionality such as specifying
min/max and, in general, a more complete understanding of the field.

The project provides:

- A field type 'accession\_reference'.
- A form Element type 'accession\_reference\_widget'.
- A field widget.
- A field formatter.

## Requirements
This module requires no Drupal modules outside of Drupal core.

## Installation
Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
The module provides a new field type called Accession Reference, and has no
UI until a field of that type exists. Each field of that type is configured
using the two field settings forms: field display and field storage.

The field widget settings control display of the value when the value is
rendered, such as whether to zero-pad numbers, while the field storage
settings control the permitted minimum and maximum value for the component
parts of the reference.

## Compatibility
Drupal 10 is supported.

## Maintainers and Author

Written by R.Ivimey-Cook (c) 2023.
 - Ruth Ivimey-Cook [rivimey](https://www.drupal.org/u/rivimey)

